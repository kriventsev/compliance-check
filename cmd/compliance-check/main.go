package main

import (
	"fmt"
	"log"

	"bitbucket.org/kriventsev/compliance-check/internal/pkg/api"
	"bitbucket.org/kriventsev/compliance-check/internal/pkg/configuration"
	"bitbucket.org/kriventsev/compliance-check/internal/pkg/logger"
)

func main() {

	config, err := configuration.GetViperConfig()
	if err != nil {
		log.Fatalf("Failed to initialize configuration: %s", err.Error())
	}

	logger, sugar, err := logger.GetZapLogger(
		config.Logger.Path,
		config.Logger.FileName,
		config.Logger.MaxSize,
	)
	if err != nil {
		log.Fatalf("Failed to initialize logger: %s", err.Error())
	}

	server, err := api.GetServer(
		config,
	)
	if err != nil {
		sugar.Fatalw("Failed to init server", "error", err.Error())
		return
	}
	server.Logger = logger
	server.Sugar = sugar

	serverSocket := fmt.Sprintf("%v:%v", config.Web.Host, config.Web.Port)
	server.Sugar.Infof("Service is running on socket %v",
		serverSocket,
	)
	if err = server.Server.Start(serverSocket); err != nil {
		sugar.Fatalw("Failed to start server", "error", err.Error())
	}
}
