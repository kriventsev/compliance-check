package api

import (
	"errors"
	"net/http"

	"bitbucket.org/kriventsev/compliance-check/internal/pkg/configuration"
	midwar "bitbucket.org/kriventsev/compliance-check/internal/pkg/middleware"
	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"

	"go.uber.org/zap"
)

//ComplianceServer authentication Server
type ComplianceServer struct {
	Server *echo.Echo
	Config *configuration.AuthenticatorConfig
	Sugar  *zap.SugaredLogger
	Logger *zap.Logger
}

//GetServer create new instance of spork server
func GetServer(config *configuration.AuthenticatorConfig) (*ComplianceServer, error) {

	//check if config is not null pointer
	if config == nil {
		return nil, errors.New("Provide non-nil config")
	}

	//init logger
	logger := initLogger()
	sugar := logger.Sugar()

	//init server
	s := &ComplianceServer{
		Server: echo.New(),
		Config: config,
		Logger: logger,
		Sugar:  sugar,
	}
	s.registerHandlers()
	s.registerMiddlewares()

	return s, nil
}

//RegisterHandlers register all handlers in one function
func (s *ComplianceServer) registerHandlers() {

	//helthcheck check server status
	s.Server.GET("/helthcheck", func(c echo.Context) error {
		type helthcheck struct {
			Status interface{} `json:"status"`
		}
		return c.JSON(http.StatusOK, &helthcheck{Status: "alive"})
	})
}

//RegisterMiddlewares use jwt middleware for secure Server
func (s *ComplianceServer) registerMiddlewares() {

	s.Server.Use(middleware.Recover())
	s.Server.Use(middleware.CORSWithConfig(middleware.CORSConfig{
		AllowOrigins:     []string{"http://localhost:3000"},
		AllowMethods:     []string{"OPTIONS", "GET", "PUT", "POST", "DELETE", "HEAD"},
		AllowCredentials: true,
	}))
	s.Server.Use(midwar.LogMiddleware(s.Sugar))
}

//initLogger initialize zap logger
func initLogger() *zap.Logger {

	logger, _ := zap.NewDevelopment()

	return logger
}
