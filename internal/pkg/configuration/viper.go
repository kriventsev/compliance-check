package configuration

import (
	"github.com/juju/errors"

	"github.com/spf13/viper"
	"gopkg.in/go-playground/validator.v9"
)

//GatewayConfig launch parameters structure
type AuthenticatorConfig struct {
	Web struct {
		Host      string `validate:"required"`
		Port      int    `validate:"required"`
		Transport string `validate:"required"`
	}
	Logger struct {
		FileName string `validate:"required"`
		Path     string `validate:"required"`
		MaxSize  int    `validate:"required"`
	}
}

//GetViperConfig load configuration from file, env and flags with viper package
func GetViperConfig() (*AuthenticatorConfig, error) {
	config := &AuthenticatorConfig{}

	//config file name and location
	viper.SetConfigName("config")
	viper.AddConfigPath(".")

	//read configuration file
	if err := viper.ReadInConfig(); err != nil {
		return nil, errors.Annotate(err, "Failed to load configuration file")
	}

	//unmarshal configuration file
	if err := viper.Unmarshal(config); err != nil {
		return nil, errors.Annotate(err, "Failed to unmarshal configuration file")
	}

	return config, nil
}

//validate config parameters validation
func (c *AuthenticatorConfig) validate() error {
	var err error

	validate := validator.New()
	if err := validate.Struct(c.Web); err != nil {
		return errors.Annotate(err, "Failed to validate Web configuration")
	}
	if err := validate.Struct(c.Logger); err != nil {
		return errors.Annotate(err, "Failed to validate Logger configuration")
	}

	return err
}
