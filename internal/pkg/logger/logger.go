package logger

import (
	"errors"

	"github.com/natefinch/lumberjack"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

//GetZapLogger initialize zap logger
func GetZapLogger(directory string, filename string, maxSize int) (*zap.Logger, *zap.SugaredLogger, error) {

	//check if config is not null pointer
	if directory == "" || filename == "" || maxSize <= 0 {
		return nil, nil, errors.New("Provide non-nil config")
	}

	writerSyncer := getLogWriter(directory, filename, maxSize)
	encoder := getEncoder()
	core := zapcore.NewCore(encoder, writerSyncer, zapcore.DebugLevel)

	logger := zap.New(core, zap.AddCaller())
	sugar := logger.Sugar()
	return logger, sugar, nil
}

func getEncoder() zapcore.Encoder {

	encoderConfig := zap.NewProductionEncoderConfig()
	encoderConfig.EncodeTime = zapcore.ISO8601TimeEncoder
	encoderConfig.EncodeLevel = zapcore.CapitalLevelEncoder

	return zapcore.NewConsoleEncoder(encoderConfig)
}

func getLogWriter(directory string, filename string, maxSize int) zapcore.WriteSyncer {

	lumberJackLogger := &lumberjack.Logger{
		Filename:   directory + "/" + filename,
		MaxSize:    maxSize,
		MaxBackups: 5,
		MaxAge:     30,
		Compress:   true,
	}
	return zapcore.AddSync(lumberJackLogger)
}
